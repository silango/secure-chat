# Emil Landron

from twisted.internet.protocol import Protocol, Factory
from twisted.internet import reactor, ssl

class ChatProtocol(Protocol):
    
    name = None
    loggedIn = False
    retryLeft = 3
    
    def connectionMade(self):
        
        self.message("Welcome to CSc Security Chat \n")
        self.message("Enter 'username;password' without quotes.\n")
        self.message("")

        self.factory.clients.append(self)
        
        self.loggedIn = False
        self.retryLeft = 3
        self.name = "None"
        
        print "Connection from: "+ self.transport.getPeer().host #print client IP
        
        
    def connectionLost(self, reason):
        self.sendMessage("-- %s left." % self.name)
        
        if not self.name == "None":
            print "Connection lost: "+ self.name
        else: 
            print "Connection lost: "+ self.transport.getPeer().host
            
        self.factory.clients.remove(self)
        
        
    def dataReceived(self, line):
        line = line.strip()      
        if not self.loggedIn:
            a = line.split(';')
            if len(a) == 2:
                user = a[0]
                password = a[1]
                self.validateUser(user,password)
            else:
                self.message("Username and Password with wrong format")
                self.message("")
        
        else:
            self.sendMessage(self.name + ": "+ line)
            
    def message(self, message):
        self.transport.write(message + "\n")
    
    
    def validateUser(self, user, password):     
        if (user == "emil") and (password == "123"):
            self.loggedIn = True
            self.name = "Emil"            
            
        elif (user == 'jose') and (password == 'abc'):
            self.loggedIn = True
            self.name = "Jose"  
                      
        elif (user == 'mauricio') and (password == 'zxc'):
            self.loggedIn = True
            self.name = "Mauricio"
            
        if (self.loggedIn == True):
            self.sendMessage("-- %s joined." % self.name)
            print "Connection from: " + self.transport.getPeer().host + " is " + self.name
            self.sendMessage(" ")
        else:
            
            if self.retryLeft < 1:
                self.message("No more retry, goodbye!")
                print "Connection "+ self.transport.getPeer().host + " was kick out because of too many retries"
                self.transport.loseConnection()
                
            else:
                self.message("User or password not valid. Retries left: " + str(self.retryLeft))
                self.message(" ")
                self.retryLeft = self.retryLeft - 1
                   
    def sendMessage(self, message):
        for client in self.factory.clients:
            if client.loggedIn:
                client.message(message)
        
        
if __name__ == "__main__":
    
    factory = Factory()
    factory.protocol = ChatProtocol
    factory.clients = []
    reactor.listenSSL(8803, factory, ssl.DefaultOpenSSLContextFactory('privkey.pem', 'cacert.pem'))
    
    print "Chat Server Running..."
    reactor.run()